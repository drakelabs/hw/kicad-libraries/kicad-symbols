# KiCad Symbol Library

It's an awesome power that in KiCad's intended workflow is the ability to
quickly create schematic symbols, PCB footprints, and even incorporate 3D
models, as needed.

However, especially with something with a ridiculous number of pins with
multiple uses like the Teensy series, there's no shame in using another's
library (or the KiCad's team library.)

## Purpose

This is merely a collection for everyone who has need of symbols, footprints,
or CAD (source) for a project. All are freely, as in "libre", licensed, unlike
unnamed ventures which require your data in trade.

So have at it, comrades. No email or first-born required, just pass on
changes/improvements to whomever you redistribute to.

## Parts

### Teensy All-Pins All The Time Project

In its current state, the library includes:
* Teensy 4.1 - All Pins

#### Purpose

The Teensy series, particularly the big boards, are infamous for have many,
many, _many_ multifunction IO pins and pads.

To take some of the sting out of creating new schematic symbols all of the
time; which, admittedly, is a cool part of KiCad, the ability to easily create
parts instead of waiting on someone else to do it, this library has been
created and will grow over time. There are many strategies for dealing with
the multifunction nature of the Teensy's GPIOs, and we may take advantage of
others.

For the time being, here is the _complete_ KiCad 4.1, which pins matching (a
personal preference.) Again using the strength of KiCad, it's even possible to
use this as a start to your preferred schematic style.

### Version

These are made for KiCad 6.99, which may be a problem for some. Hopefully,
scripts made by others will make this usable in older versions.

## License

© Source code daemonburrito 2022

All source code licensed under the AGPL 3.0 or greater.

Notice of Merchantability and Fitness

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.
